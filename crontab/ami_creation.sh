#!/bin/bash
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
bold=$(tput -Txterm bold);
normal=$(tput -Txterm sgr0);
now=$(date +"%d_%m_%Y")
echo "${bold} ============================================================================================ ${normal}"
echo "${bold} ******************* CREATING AMI FOR SonyBT-PROD-AppSrv (i-0b53707ddabc69b22) AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_details="$(sudo aws ec2 create-image --instance-id i-0b53707ddabc69b22 --name "SonyBT-PROD-AppSrv-$now" --description "SonyBT-PROD-AppSrv $now" --no-reboot)";
ami_id="$(echo $ami_details | jq '.ImageId' | tr -d '"')";
sudo aws ec2 create-tags --resources $ami_id --tags Key=Name,Value=SonyBT-PROD-AppSrv-$now
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
echo "${bold} ******************* AMI CREATION SUCCESSFUL. AMI ($ami_id) IS AVAILABLE AT $TODAY ( $TODAYINDIA ) ******************* ${normal}";

echo "${bold} ******************* CREATING AMI FOR SonyBT-PROD-MBOSrv (i-095a6d77ddb67a65e) AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_details="$(sudo aws ec2 create-image --instance-id i-095a6d77ddb67a65e --name "SonyBT-PROD-MBOSrv-$now" --description "SonyBT-PROD-MBOSrv $now" --no-reboot)";
ami_id="$(echo $ami_details | jq '.ImageId' | tr -d '"')";
sudo aws ec2 create-tags --resources $ami_id --tags Key=Name,Value=SonyBT-PROD-MBOSrv-$now
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
echo "${bold} ******************* AMI CREATION SUCCESSFUL. AMI ($ami_id) IS AVAILABLE AT $TODAY ( $TODAYINDIA ) ******************* ${normal}";

echo "${bold} ******************* CREATING AMI FOR SonyBT-PROD-NFSSrv (i-0b8025284c43c0a79) AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_details="$(sudo aws ec2 create-image --instance-id i-0b8025284c43c0a79 --name "SonyBT-PROD-NFSSrv-$now" --description "SonyBT-PROD-NFSSrv $now" --no-reboot)";
ami_id="$(echo $ami_details | jq '.ImageId' | tr -d '"')";
sudo aws ec2 create-tags --resources $ami_id --tags Key=Name,Value=SonyBT-PROD-NFSSrv-$now
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
echo "${bold} ******************* AMI CREATION SUCCESSFUL. AMI ($ami_id) IS AVAILABLE AT $TODAY ( $TODAYINDIA ) ******************* ${normal}";

echo "${bold} ******************* CREATING AMI FOR SonyBT-PROD-VarnishSrv (i-0c7403d3ef47445dd) AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_details="$(sudo aws ec2 create-image --instance-id i-0c7403d3ef47445dd --name "SonyBT-PROD-VarnishSrv-$now" --description "SonyBT-PROD-VarnishSrv $now" --no-reboot)";
ami_id="$(echo $ami_details | jq '.ImageId' | tr -d '"')";
sudo aws ec2 create-tags --resources $ami_id --tags Key=Name,Value=SonyBT-PROD-VarnishSrv-$now
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
echo "${bold} ******************* AMI CREATION SUCCESSFUL. AMI ($ami_id) IS AVAILABLE AT $TODAY ( $TODAYINDIA ) ******************* ${normal}";
