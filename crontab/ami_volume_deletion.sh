#!/bin/bash
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAY=$(TZ=":US/Eastern" date)
bold=$(tput -Txterm bold);
normal=$(tput -Txterm sgr0);
now=$(date --date "15 days ago" +"%d_%m_%Y");
echo "${bold} =========================================================================== ${normal}"
echo "${bold} ******************* DELETING AMI FOR SonyBT-PROD-AppSrv AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-AppSrv-$now" --query 'Images[*].{ID:ImageId}' | jq '.[0].ID' | tr -d '"')";
snapshot_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-AppSrv-$now" --query 'Images[*].BlockDeviceMappings[*].Ebs.{SID:SnapshotId}' | jq '.[0][0].SID' | tr -d '"')";

sudo aws ec2 deregister-image --image-id $ami_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING AMI WITH ID $ami_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED AMI with AMI ID $ami_id and Name SonyBT-PROD-AppSrv-$now AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

sudo aws ec2 delete-snapshot --snapshot-id $snapshot_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING SNAPSHOT WITH ID $snapshot_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED SNAPSHOT with Snapshot ID $snapshot_id AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

echo "${bold} =========================================================================== ${normal}"
echo "${bold} ******************* DELETING AMI FOR SonyBT-PROD-MBOSrv AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-MBOSrv-$now" --query 'Images[*].{ID:ImageId}' | jq '.[0].ID' | tr -d '"')";
snapshot_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-MBOSrv-$now" --query 'Images[*].BlockDeviceMappings[*].Ebs.{SID:SnapshotId}' | jq '.[0][0].SID' | tr -d '"')";

sudo aws ec2 deregister-image --image-id $ami_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING AMI WITH ID $ami_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED AMI with AMI ID $ami_id and Name SonyBT-PROD-MBOSrv-$now AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

sudo aws ec2 delete-snapshot --snapshot-id $snapshot_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING SNAPSHOT WITH ID $snapshot_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED SNAPSHOT with Snapshot ID $snapshot_id AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

echo "${bold} =========================================================================== ${normal}"
echo "${bold} ******************* DELETING AMI FOR SonyBT-PROD-NFSSrv AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-NFSSrv-$now" --query 'Images[*].{ID:ImageId}' | jq '.[0].ID' | tr -d '"')";
snapshot_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-NFSSrv-$now" --query 'Images[*].BlockDeviceMappings[*].Ebs.{SID:SnapshotId}' | jq '.[0][0].SID' | tr -d '"')";

sudo aws ec2 deregister-image --image-id $ami_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING AMI WITH ID $ami_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED AMI with AMI ID $ami_id and Name SonyBT-PROD-NFSSrv-$now AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

sudo aws ec2 delete-snapshot --snapshot-id $snapshot_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING SNAPSHOT WITH ID $snapshot_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED SNAPSHOT with Snapshot ID $snapshot_id AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

echo "${bold} =========================================================================== ${normal}"
echo "${bold} ******************* DELETING AMI FOR SonyBT-PROD-VarnishSrv AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
ami_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-VarnishSrv-$now" --query 'Images[*].{ID:ImageId}' | jq '.[0].ID' | tr -d '"')";
snapshot_id="$(sudo aws ec2 describe-images --filters "Name=tag:Name,Values=SonyBT-PROD-VarnishSrv-$now" --query 'Images[*].BlockDeviceMappings[*].Ebs.{SID:SnapshotId}' | jq '.[0][0].SID' | tr -d '"')";

sudo aws ec2 deregister-image --image-id $ami_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING AMI WITH ID $ami_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED AMI with AMI ID $ami_id and Name SonyBT-PROD-VarnishSrv-$now AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi

sudo aws ec2 delete-snapshot --snapshot-id $snapshot_id
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
if [ $? -ne 0 ]
then
    echo "${bold} ########## ERROR OCCURED WHILE DELETING SNAPSHOT WITH ID $snapshot_id AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        #SEND EMAIL
else
    echo "${bold} ******************* DELETED SNAPSHOT with Snapshot ID $snapshot_id AT $TODAY ( $TODAYINDIA ) ******************* ${normal}"
fi
